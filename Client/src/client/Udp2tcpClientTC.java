package client;
import java.io.*;
import java.net.*;

import java.util.Arrays;

public class Udp2tcpClientTC {
    private static int inUDPPort=4558;
    private static int outTCPPort=21111;
    private static String ipFwdAddress;
    private static DatagramSocket serverSocket;
    private static DatagramPacket sendPacket = null;
    private static Socket tcpClientSocket;
    private static DataOutputStream outToServer =null;
    private static byte[] udpInBuffer = new byte[64000];
    private static byte[] tcpOutBuffer = new byte[64000];
    private static int udpInPayloadLength;
    private static int tcpOutPayloadLength;
    private static boolean ConnectedToTCPServer=false;
    private static boolean ListeningUDPServer=false;
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_RESET = "\u001B[0m";
    public Udp2tcpClientTC() {
        super();
    }

    public static void main(String[] args) {
        
        if (args.length==0||args.length==1||args.length==2){
            System.out.println("need 3 arguments [Input UDP port] [Output TCP port] [ip address] to forward\neg java -jar udp2tcp_tc.jar 4550 21111 127.0.0.1");
            return;
            }
        
        Udp2tcpClientTC udp2tcpClientTC = new Udp2tcpClientTC();
        
        
        
        inUDPPort = Integer.parseInt(args[0]);
        outTCPPort = Integer.parseInt(args[1]);
        ipFwdAddress = args[2];
        

        while (true){
        while (ConnectedToTCPServer == false){
            try {
                if (ListeningUDPServer==false){
                    try { 
                        serverSocket = new DatagramSocket(inUDPPort);
                        System.out.println(ANSI_GREEN+"Listening on UDP Port "+inUDPPort+ANSI_RESET);
                        ListeningUDPServer = true;
                    }
                    catch (SocketException e) {
                                    System.out.println("Failed to initialize udp socket ... \nEnding program on" +
                                        " error :" + e.toString());
                                    return;
                }
                
            } 
                tcpClientSocket = new Socket(ipFwdAddress, outTCPPort);
                outToServer = new DataOutputStream(tcpClientSocket.getOutputStream());
                System.out.println(ANSI_GREEN+"connected to server address "+ipFwdAddress+" TCP port "+outTCPPort+ANSI_RESET);
                ConnectedToTCPServer = true;
                break;   
                //return;
            } catch (UnknownHostException e) {
                System.out.println("Failed to initialize tcp client socket ... \nEnding program on" +
                    " error :" + e.toString());
            } catch (IOException e) {
                System.out.println("Failed to initialize tcp client socket ... \nEnding program on" +
                    " error :" + e.toString());
            }
            System.out.println(ANSI_RED+"No connection to TCP Server.... \nretrying to connect"+ANSI_RESET);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

        }
        try {
        while (true){
            DatagramPacket receivePacket = new DatagramPacket(udpInBuffer, udpInBuffer.length);
            serverSocket.receive(receivePacket);
            udpInPayloadLength = receivePacket.getLength();
            System.out.println(ANSI_GREEN+"Recieved Bytes UDP: "+udpInPayloadLength+ANSI_RESET);
            tcpOutPayloadLength = udpInPayloadLength;
            tcpOutBuffer = Arrays.copyOf(receivePacket.getData(),udpInPayloadLength);
            //the protocol to tcp server is the following 
            //Client connects to server and then sends integer informing about the size
            //of the of data to follow.
            //ATTENTION i have to make server to send an OK so that client verifies that command 
            //has been forwarder.
            outToServer.writeInt(tcpOutPayloadLength);
            //now we send tcpOutPayloadLength bytes
            outToServer.write(tcpOutBuffer, 0, tcpOutPayloadLength);
            System.out.println(ANSI_RED+"Sent     Bytes TCP: "+tcpOutPayloadLength+ANSI_RESET);
            }
        }catch (Exception e){
            System.out.println("Error  lost communication to tcp server ... \nEnding program on" +
                                        " error :" + e.toString());
            //serverSocket.close();
            try {
                tcpClientSocket.close();
                //return;
            } catch (IOException f) {
                System.out.println("Failed to close tcp client socket  ... \nEnding program on" +
                                            " error :" + e.toString());
                return;
            }
        }
        System.out.println(ANSI_RED+"Retrying to connect to server ..."+ANSI_RESET);
            ConnectedToTCPServer = false;
    }
    }
}
